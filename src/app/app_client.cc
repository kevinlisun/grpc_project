
#include <iostream>
#include <memory>
#include <string>
#include <fstream>
#include <sstream>

#include <grpc++/grpc++.h>

#ifdef BAZEL_BUILD
#include "examples/protos/app.grpc.pb.h"
#else
#include "app.grpc.pb.h"
#endif

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using app::AppRequest;
using app::AppReply;
using app::Transfer;

class TransferClient {
 public:
  TransferClient(std::shared_ptr<Channel> channel)
      : stub_(Transfer::NewStub(channel)) {}

  // Assembles the client's payload, sends it and presents the response back
  // from the server.
  std::string SendData(const int file_status, const std::string & file_name, const std::string & data) {
    // Data we are sending to the server.
      AppRequest request;
      request.set_file_status(file_status);
      request.set_file_name(file_name);
      request.set_str_data(data);

      // Container for the data we expect from the server.
      AppReply reply;

      // Context for the client. It could be used to convey extra information to
      // the server and/or tweak certain RPC behaviors.
      ClientContext context;

      // The actual RPC.
      Status status = stub_->SendData(&context, request, &reply);

      // Act upon its status.
      if (status.ok()) {
        return reply.msg();
      } else {
      std::cout << status.error_code() << ": " << status.error_message() << std::endl;
      return "RPC failed";
      }
    


 }
 private:
  std::unique_ptr<Transfer::Stub> stub_;
};

int main(int argc, char** argv) {
  // Instantiate the client. It requires a channel, out of which the actual RPCs
  // are created. This channel models a connection to an endpoint (in this case,
  // localhost at port 50051). We indicate that the channel isn't authenticated
  // (use of InsecureChannelCredentials()).
  TransferClient transfer(grpc::CreateChannel(
      "localhost:50051", grpc::InsecureChannelCredentials()));

  std::string file_name;

  if (argc > 1)
  {
    std::stringstream fn;

    fn << argv[1];
    file_name = fn.str();
  }
  else
    file_name = "001.txt";

  std::ifstream file("./client_data/" + file_name);
  if (file.good() == 0)
  {
    std::cout << "file "+ file_name + " is not existed!" << std::endl;
    return 0;
  }

  std::string line;

  while (std::getline(file, line))
  {
    int file_status = 1;
    std::string reply = transfer.SendData(file_status, file_name, line);
    std::cout << "feedback received: " << reply << std::endl;
  }
  int file_status = 0;
  std::string reply = transfer.SendData(file_status, file_name, line);
  std::cout << "feedback received: " << reply << std::endl;

  return 0;
}
