
#include <iostream>
#include <memory>
#include <string>
#include <sstream>
#include <fstream>

#include <grpc++/grpc++.h>

#ifdef BAZEL_BUILD
#include "examples/protos/app.grpc.pb.h"
#else
#include "app.grpc.pb.h"
#endif

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using app::AppRequest;
using app::AppReply;
using app::Transfer;


int msg_count = 0;

// Logic and data behind the server's behavior.
class TransferServiceImpl final : public Transfer::Service 
{
  Status SendData(ServerContext* context, const AppRequest* request,
                  AppReply* reply) override 
  {
    std::string file_name("./server_data/" + request->file_name());

    std::ofstream file;
    if (msg_count == 0)
      file.open(file_name, std::ofstream::out);
    else
      file.open(file_name, std::ofstream::app);


    if (request->file_status() == 1) //request->msg
    {
      //std::string tmp_str(request->str_data);
      std::cout << "-";

      file << (request->str_data()) << "\n";

      std::stringstream ss;
      ss << ++msg_count;

      std::string msg("File " + request->file_name() +" Sending...");
    
      // do something important here

      reply->set_msg(msg);
      return Status::OK;
    }
    else if(request->file_status() == 0)
    {
      std::cout << ">done!" << std::endl;

      file.close();
      msg_count = 0;

      reply->set_msg("File " + request->file_name() + " Sent.");
      return Status::OK;
    }
    else
    {
      std::cout << "unknown message status! [" << request->file_status() << "]" << std::endl;
    }
  }
};

void RunServer() {
  std::string server_address("0.0.0.0:50051");
  TransferServiceImpl service;

  ServerBuilder builder;
  // Listen on the given address without any authentication mechanism.
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  // Register "service" as the instance through which we'll communicate with
  // clients. In this case it corresponds to an *synchronous* service.
  builder.RegisterService(&service);
  // Finally assemble the server.
  std::unique_ptr<Server> server(builder.BuildAndStart());
  std::cout << "Server listening on " << server_address << std::endl;

  // Wait for the server to shutdown. Note that some other thread must be
  // responsible for shutting down the server for this call to ever return.
  server->Wait();
}

int main(int argc, char** argv) {
  RunServer();

  return 0;
}
