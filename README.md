# gRPC C++ File Exchange Project

## Prerequisites

### Install ProtoBuf v3
Make sure you have installed Protobuf on your system. Follow the instructions here:
https://github.com/google/protobuf/blob/master/src/README.md

### Install gRPC
Make sure you have installed gRPC on your system. Follow the instructions here:
[https://github.com/grpc/grpc/blob/master/INSTALL](../../../INSTALL.md).


## Compile (Ubuntu)

-> git clone git@bitbucket.org:kevinlisun/grpc_project.git

-> cd grpc_project/src/app

-> make

## Test
-> ./app_server

Open another terminal

-> ./app_client 001.txt

Now you should see 001.txt in folder /src/app/server_data


## Description
// The transfering service definition.
service Transfer {
  // Sends a transfering
  rpc SendData (AppRequest) returns (AppReply) {}
}

// The request message containing the user's name.
message AppRequest {
  int32 file_status = 1; // 1 means "sending", and 0 refers "finished"
  string file_name = 2;
  string str_data = 3; // split the large file into lines and transfer it line by line
}

// The response message containing the greetings
message AppReply {
  string msg = 1;
}